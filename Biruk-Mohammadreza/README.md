==================================
tp2 repository Information
================================

All notebooks are run using jupyter notebook in pyspark mode. We configured PySpark with jupyter
notebook.

Credentials must be configured in .aws folder to run all notebooks except DigitRecognitionTraining.

The results are stored inside Result Folder. It contains the classification result of SVM, MLP and random forest classifiers, sparkwordcountresult and Logs of parameter tuenning for the three ML models


The dataset for MLLib is located in the folder MLib_Demo


We also included the lambda_function packaging folder. the instructions to package lambda function and its dependencies. 

ML_models folder contains the pickled model of all three ML algorithms

Code folder contains all scripts and notebooks that are used in TP2

===========================================================
!!THANK YOU!!
==========================================================
