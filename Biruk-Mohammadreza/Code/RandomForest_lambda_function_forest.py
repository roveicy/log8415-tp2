import pandas as pd #Pandas is used to load dataset into a 2D array to be classified by our model
import pickle  #pickle is used to store our trained model
import boto3
import sklearn
import json


def lambda_handler(event, context):
    #Load the model from s3 bucket
    s3 = boto3.client('s3')
    bucket = 'mbtp2'
    key ='MLP.pkl'
    download_path_model = '/tmp/'+key
    #download_path='~/lambda/'

    s3.download_file ( bucket , key , download_path_model ) #download file to tmp directory

    #DownLoad the dataset from s3 bucket
    key ='ds1Test.csv'
    download_path_test = '/tmp/'+key
    #download_path='~/lambda/'
    s3.download_file ( bucket , key , download_path_test ) #download file to tmp directory

    #load dataset from local file
    data_test=pd.read_csv(download_path_test, header=None, sep=',')
    #Load the pickled model
    pkl2=open(download_path_model,"rb")
    model= pickle.load(pkl2)
    result=model.predict(data_test)
    #create a tempoory file to store the result

    result_file='/tmp/MLP_Res.csv'
    resFile=open(result_file,"w+")
    data=""
    for pred in result:
    	data+=str(pred)+"\n"
    resFile.write(data)
    resFile.close()

    #upload this local file to s3
    s3.upload_file(result_file,bucket,bucket+"MLP_Res.csv")
    return {
        'statusCode': 200, #response code
        'body':json.dumps("MLP Classifier executed successfully") #body of response message
    }



